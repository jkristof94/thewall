package com.example.thewall;

import com.example.thewall.beans.User;
import com.example.thewall.services.UserServices;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServicesTest {

  @Autowired
  UserServices userServices;

  @Test
  public void findByEmail() {
    User user = userServices.findByEmail(null);
    assertEquals(null, user);
    user = userServices.findByEmail("");
    assertEquals(null, user);
    user = userServices.findByEmail("djflkjsdlfjsdlfjsdlf");
    assertEquals(null, user);
    user = userServices.findByEmail("jkristof94@gmail.com");
    assertEquals("jkristof94@gmail.com", user.getEmail());
  }

  @Test
  public void existsByUsername() {
    boolean result = userServices.existsByUsername(null);
    assertEquals(false, result);
    result = userServices.existsByUsername("");
    assertEquals(false, result);
    result = userServices.existsByUsername("djflkjsdlfjsdlfjsdlf");
    assertEquals(false, result);
    result = userServices.existsByUsername("jkristof94");
    assertEquals(true, result);
  }

  @Test
  public void createUser() {

  }

  @Test
  public void updateUser() {
  }
}