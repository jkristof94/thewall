function upload(file) {
    var reader = new FileReader();
    reader.onload = function (e) {
            document.getElementById('profilePicture').src = e.target.result;
            document.getElementById('profile_picture').value = e.target.result;
    };
    reader.readAsDataURL(file);
};

function validateForm() {
    var form = document.forms["user-detail-form"];

    password = form["password"];
    password_again = form["password_again"];
    error_label = document.getElementById('error_label');

    if (password.value != password_again.value) {
        password.style.borderColor = "#AB0010";
        password_again.style.borderColor = "#AB0010";
        error_label.innerHTML = "Passwords do not match!";
        error_label.style.display = "block";
        return false;
    }

    return true;
}

function checkUsername(uname){
    $.ajax({
        type: "POST",
        url: "/checkUsername",
        data: { username: uname },
        success: function(result){
            document.getElementById("username").style.borderColor = result ? "#AB0010" : "#10AB00";
        }
    });
}