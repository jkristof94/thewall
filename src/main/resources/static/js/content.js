
function toggleIcon(item, attribFrom, attribTo, color, toFill) {
	if($(item).attr("class").indexOf(attribFrom) >= 0) {
		$(item).css('color', color);
		$(item).toggleClass(attribFrom + " " + attribTo);
		if(toFill) {
			$(item).toggleClass("far fas");
		}
	} else {
		$(item).css('color', "#AAA");
		$(item).toggleClass(attribTo + " " + attribFrom);
		if(toFill) {
			$(item).toggleClass("fas far");
		}
	}
}

function toggleLove(item) {
	toggleIcon(item, "unloved", "loved", "#AB0010", true);
}

function toggleShare(item) {
	toggleIcon(item, "unshared", "shared", "#4169E1", false);
}