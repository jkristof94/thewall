$(
	function(){
		if(!$("#navbar").length > 0) {
			$(document.body).append('<div id="navbar"></div>');
			$("#navbar").load("navbar.html");
		}
	}
);

function toggleBrickVisibility(item) {
	var header = item.parentNode.parentNode;
	var content = header.parentNode.getElementsByClassName("content")[0];
	var toggleIcon = header.getElementsByClassName("toggler")[0];
	$(content).slideToggle("fast");
	if(content.clientHeight != 0) {
		header.style.borderRadius = "5px 5px 5px 5px";
		$(toggleIcon).toggleClass("fas fa-minus fas fa-plus");
	} else {
		header.style.borderRadius = "5px 5px 0px 0px";
		$(toggleIcon).toggleClass("fas fa-plus fas fa-minus");
	}
}