CREATE SCHEMA common
    AUTHORIZATION postgres;

CREATE TABLE common.users
(
    username text NOT NULL,
    email text NOT NULL,
    password text NOT NULL,
    picture_hash text,
    role text NOT NULL,
    enabled boolean NOT NULL,
    full_name text,
    birthday DATE,
    CONSTRAINT users_pkey PRIMARY KEY (email)
);

CREATE TABLE common.following (
    follower_email text NOT NULL REFERENCES common.users(email),
    following_email text NOT NULL REFERENCES common.users(email),
    CONSTRAINT following_pkey PRIMARY KEY (follower_email, following_email)
);