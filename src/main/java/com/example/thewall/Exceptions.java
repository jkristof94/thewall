package com.example.thewall;

public class Exceptions {

  public static class DuplicateUserException extends LoggedExceptions {
    public DuplicateUserException() {
      super("A User with the given Username or Email already exists!");
    }
  }

  public static class PasswordsDontMatchException extends LoggedExceptions {
    public PasswordsDontMatchException() {
      super("Passwords do not match!");
    }
  }

}
