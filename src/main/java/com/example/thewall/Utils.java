package com.example.thewall;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

public class Utils {

  private static final Md5PasswordEncoder encoder = new Md5PasswordEncoder();

  public static String getMD5(String password) {
    return encoder.encodePassword(password, "");
  }
}
