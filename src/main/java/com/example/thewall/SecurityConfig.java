package com.example.thewall;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableAutoConfiguration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private static String LOGIN_QUERY
      = "SELECT email, password, enabled FROM common.users WHERE email=?";
  private static String AUTH_QUERY
      = "SELECT email, 'ROLE_USER' FROM common.users WHERE email=?";

  @Autowired
  DataSource dataSource;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    //@formatter:off
    http
        .authorizeRequests()
          .antMatchers("/css/**", "/images/**", "/js/**", "/register**", "/lost**")
          .permitAll()
          .and()
        .authorizeRequests()
          .antMatchers("/**")
          .authenticated()
          .and()
        .formLogin()
          .loginPage("/login")
          .permitAll()
          .and()
        .logout()
          .logoutUrl("/logout")
          .permitAll()
          .and()
        .csrf()
          .disable();
    //@formatter:on
  }

  /**
   * Configure the authentication manager.
   * @param auth {@link AuthenticationManagerBuilder}
   * @throws Exception {@link Exception}
   */
  @Autowired
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.jdbcAuthentication()
        .dataSource(dataSource)
        .usersByUsernameQuery(LOGIN_QUERY)
        .authoritiesByUsernameQuery(AUTH_QUERY)
        .passwordEncoder(new Md5PasswordEncoder());
  }
}
