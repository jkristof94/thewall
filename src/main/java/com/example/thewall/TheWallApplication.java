package com.example.thewall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TheWallApplication {

  public static void main(String[] args) {
    SpringApplication.run(TheWallApplication.class, args);
  }

}
