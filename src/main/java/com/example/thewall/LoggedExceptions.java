package com.example.thewall;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggedExceptions extends Exception {

  Logger logger = LoggerServices.getLogger();

  public LoggedExceptions(String message) {
    super(message);
    logger.log(Level.WARNING, message);
  }

  public LoggedExceptions(String message, Throwable cause) {
    super(message, cause);
    logger.log(Level.WARNING, message + " on [ " + cause.getMessage() + " ]");
  }

  public LoggedExceptions(Throwable cause) {
    super(cause);
    logger.log(Level.WARNING, "[ " + cause.getMessage() + " ]");
  }

  protected LoggedExceptions(String message, Throwable cause,
                             boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    logger.log(Level.WARNING, message + " on [ " + cause.getMessage() + " ]");
  }
}
