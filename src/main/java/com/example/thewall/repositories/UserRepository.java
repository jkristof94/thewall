package com.example.thewall.repositories;

import com.example.thewall.beans.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {

  @Query("SELECT u FROM User u "
      + "WHERE LOWER(u.email) = LOWER(:email)")
  User findByEmail(@Param("email") String email);

  @Query("SELECT u.email FROM User u "
      + "WHERE LOWER(u.email) = LOWER(:email)")
  String existsByEmail(@Param("email") String email);

  @Query("SELECT u.email FROM User u "
      + "WHERE LOWER(u.username) = LOWER(:username)")
  String existsByUsername(@Param("username") String username);

  @Query("SELECT MAX(u.email) FROM User u "
      + "WHERE LOWER(u.username) = LOWER(:username) "
      + "OR LOWER(u.email) = LOWER(:email)")
  String existsByUsernameOrEmail(@Param("username") String username, @Param("email") String email);
}
