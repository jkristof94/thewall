package com.example.thewall.repositories;

import com.example.thewall.beans.Follow;
import com.example.thewall.beans.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FollowRepository extends JpaRepository<User, Long> {

  @Query("SELECT f FROM Follow f WHERE LOWER(f.followerEmail) = LOWER(:email)")
  List<Follow> listFollowing(@Param("email") String email);

  @Query("SELECT f FROM Follow f WHERE LOWER(f.followingEmail) = LOWER(:email)")
  List<Follow> listFollowers(@Param("email") String email);

}