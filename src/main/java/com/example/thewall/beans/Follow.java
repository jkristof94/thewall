package com.example.thewall.beans;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "following", schema = "common")
public class Follow implements Serializable {

  @Id
  @Column(name = "follower_email", nullable = false, unique = true)
  private String followerEmail;

  @Id
  @Column(name = "following_email", nullable = false, unique = true)
  private String followingEmail;

  protected Follow() {}

  //<editor-fold desc="Getters/Setters equals, hashCode and toString">

  public String getFollowerEmail() {
    return followerEmail;
  }

  public void setFollowerEmail(String followerEmail) {
    this.followerEmail = followerEmail;
  }

  public String getFollowingEmail() {
    return followingEmail;
  }

  public void setFollowingEmail(String followingEmail) {
    this.followingEmail = followingEmail;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Follow follow = (Follow) o;
    return Objects.equals(followerEmail, follow.followerEmail)
        && Objects.equals(followingEmail, follow.followingEmail);
  }

  @Override
  public int hashCode() {
    return Objects.hash(followerEmail, followingEmail);
  }

  @Override
  public String toString() {
    return String.format("Follow[ %s follows %s ]", followerEmail, followingEmail);
  }

  //</editor-fold>

}
