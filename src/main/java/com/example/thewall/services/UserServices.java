package com.example.thewall.services;

import static com.example.thewall.Utils.getMD5;
import static java.sql.Date.valueOf;
import static org.springframework.util.StringUtils.isEmpty;

import com.example.thewall.Exceptions;
import com.example.thewall.beans.User;
import com.example.thewall.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class UserServices {

  @Autowired
  UserRepository userRepository;

  public User findByEmail(String email) {
    return StringUtils.isEmpty(email) ? null : userRepository.findByEmail(email);
  }

  public boolean existsByUsername(String username) {
    return StringUtils.isEmpty(username) || userRepository.existsByUsername(username) == null ? false : true;
  }

  /**
   * Create a new User with validation.
   *
   * @param email         Email
   * @param username      Username
   * @param password      Password
   * @param passwordAgain Password again
   * @throws Exceptions.DuplicateUserException
   * {@link com.example.thewall.Exceptions.DuplicateUserException}
   * @throws Exceptions.PasswordsDontMatchException
   * {@link com.example.thewall.Exceptions.PasswordsDontMatchException}
   */
  public void createUser(String email, String username, String password, String passwordAgain)
      throws Exceptions.DuplicateUserException, Exceptions.PasswordsDontMatchException {
    if (password != null && !password.equals(passwordAgain)) {
      throw new Exceptions.PasswordsDontMatchException();
    }

    if (userRepository.existsByUsernameOrEmail(username, email) != null) {
      throw new Exceptions.DuplicateUserException();
    }

    userRepository.save(new User(username, email, password));
  }

  /**
   * Update a user with validation.
   *
   * @param email         Email
   * @param username      Username
   * @param password      Password
   * @param passwordAgain Password again
   * @param birthday      Birthday
   * @param pictureHash   Picture hash
   * @throws Exceptions.DuplicateUserException
   * {@link com.example.thewall.Exceptions.DuplicateUserException}
   * @throws Exceptions.PasswordsDontMatchException
   * {@link com.example.thewall.Exceptions.PasswordsDontMatchException}
   */
  public void updateUser(String email, String username, String fullName,
                         String password, String passwordAgain, String birthday, String pictureHash)
      throws Exceptions.DuplicateUserException, Exceptions.PasswordsDontMatchException {

    final User user = userRepository.findByEmail(email);
    validate(user, username, password, passwordAgain);
    update(user, username, fullName, password, birthday, pictureHash);
  }

  /**
   * Validates a user.
   *
   * @param user          {@link User}
   * @param username      Username
   * @param password      Password
   * @param passwordAgain Password again
   * @throws Exceptions.DuplicateUserException
   * {@link com.example.thewall.Exceptions.DuplicateUserException}
   * @throws Exceptions.PasswordsDontMatchException
   * {@link com.example.thewall.Exceptions.PasswordsDontMatchException}
   */
  private void validate(User user, String username, String password, String passwordAgain)
      throws Exceptions.DuplicateUserException, Exceptions.PasswordsDontMatchException {
    if (password != null && !password.equals(passwordAgain)) {
      throw new Exceptions.PasswordsDontMatchException();
    }

    if (username != null && !user.getUsername().equals(username)
        && userRepository.existsByUsername(username) != null) {
      throw new Exceptions.DuplicateUserException();
    }
  }

  /**
   * Updates a user in the db.
   *
   * @param user        {@link User}
   * @param username    Username
   * @param fullName    Full name
   * @param password    Password
   * @param birthday    Password again
   * @param pictureHash Picture hash
   */
  private void update(User user, String username, String fullName,
                      String password, String birthday, String pictureHash) {

    user.setUsername(username);
    user.setFullName(fullName);
    user.setPictureHash(pictureHash);
    user.setBirthday(valueOf(birthday));

    if (!isEmpty(password)) {
      user.setPassword(getMD5(password));
    }

    userRepository.save(user);
  }

}
