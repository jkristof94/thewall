package com.example.thewall;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;


public class LoggerServices {

  /**
   * Creates a logger with a console handler {@link ConsoleHandler}.
   * @return The created logger
   */
  public static Logger getLogger() {
    final Handler ch = new ConsoleHandler();
    final Logger logger = Logger.getLogger(LoggerServices.class.getName());
    logger.addHandler(ch);
    return logger;
  }
}
