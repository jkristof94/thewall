package com.example.thewall.controllers;

import com.example.thewall.Exceptions;
import com.example.thewall.services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class RegistrationController {

  @Autowired
  UserServices userServices;

  @GetMapping("/register")
  public String showRegister(Model model) {
    return "register";
  }

  /**
   * Used for registering a new user into the database.
   * @param email Email
   * @param username Username
   * @param password Password
   * @param passwordAgain Password again
   * @return Redirect page
   */
  @PostMapping("/register")
  public String register(
      @RequestParam(value = "email") String email,
      @RequestParam(value = "username") String username,
      @RequestParam(value = "password") String password,
      @RequestParam(value = "password_again") String passwordAgain
  ) {
    try {
      userServices.createUser(email, username, password, passwordAgain);
    } catch (Exceptions.DuplicateUserException ex) {
      return "redirect:register?alreadyinuse";
    } catch (Exceptions.PasswordsDontMatchException ex) {
      return "redirect:register?nomatch";
    }

    return "redirect:login";
  }

}
