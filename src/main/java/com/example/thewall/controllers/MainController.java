package com.example.thewall.controllers;

import com.example.thewall.services.UserServices;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

  @Autowired
  UserServices userServices;

  @GetMapping("/login")
  public String login() {
    return "login";
  }

  @GetMapping("/logout")
  public String logout() {
    return "redirect:login?logout";
  }

  @GetMapping("/lost")
  public String lost() {
    return "lost";
  }

  @GetMapping({"/", "/index"})
  public ModelAndView index(HttpServletRequest request) {
    return new ModelAndView("index")
        .addObject("user", userServices.findByEmail(request.getRemoteUser()));
  }

}
