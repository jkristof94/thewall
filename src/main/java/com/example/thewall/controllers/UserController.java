package com.example.thewall.controllers;

import com.example.thewall.Exceptions;
import com.example.thewall.repositories.FollowRepository;
import com.example.thewall.services.UserServices;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

  @Autowired
  UserServices userServices;

  @Autowired
  FollowRepository followRepository;

  /**
   * Redirects to the user site with the correct model.
   * @param request The request containing the user
   * @return The model containing the required objects
   */
  @GetMapping("/user")
  public ModelAndView user(HttpServletRequest request) {
    final String email = request.getRemoteUser();
    return new ModelAndView("user")
        .addObject("user", userServices.findByEmail(email))
        .addObject("following", followRepository.listFollowing(email))
        .addObject("followers", followRepository.listFollowers(email));
  }

  /**
   * Used for saving the changes on the user detail page.
   * @param request The request containing the user
   * @param username Username
   * @param fullName Full name
   * @param password Password
   * @param birthday Birthday
   * @param passwordAgain Password again
   * @param profilePicture Profile picture
   * @return The model containing the required objects
   */
  @PostMapping("/save")
  public String save(
      HttpServletRequest request,
      @RequestParam(value = "username") String username,
      @RequestParam(value = "fullName") String fullName,
      @RequestParam(value = "password") String password,
      @RequestParam(value = "birthday") String birthday,
      @RequestParam(value = "password_again") String passwordAgain,
      @RequestParam(value = "profile_picture") String profilePicture
  ) {
    try {
      userServices.updateUser(request.getRemoteUser(), username, fullName, password,
          passwordAgain, birthday, profilePicture);
    } catch (Exceptions.DuplicateUserException ex) {
      return "redirect:user?username_taken";
    } catch (Exceptions.PasswordsDontMatchException ex) {
      return "redirect:user?nomatch";
    }

    return "redirect:user";
  }

  /**
   * AJAX request endpoint for user existence check.
   * @param username Username
   * @param request Request
   * @return Whether or not the specified user exists in the database
   */
  @PostMapping(value = "/checkUsername", produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody
  String checkUsername(@RequestParam("username") String username, HttpServletRequest request) {
    String currentUsername = userServices.findByEmail(request.getRemoteUser()).getUsername();
    return !currentUsername.equals(username)
        && userServices.existsByUsername(username) ? "true" : "false";
  }

}
